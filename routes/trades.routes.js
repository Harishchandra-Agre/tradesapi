const express = require('express');
const router = express.Router();
const tradeController = require('../controller/trade.controller');

router.post('/', tradeController.addTrade);
router.get('/', tradeController.findTrades);
router.get('/:id', tradeController.findTradeById);
router.put('/:id', tradeController.updateTrade);
router.delete('/:id', tradeController.deleteById);

module.exports = router;