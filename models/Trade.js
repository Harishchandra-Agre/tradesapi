const Sequelize = require('sequelize');
const db = require('../Connection/database');

const Trade = db.define('trades', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    type: {
        type: Sequelize.ENUM('BUY', 'SELL'),
        allowNull: false,

    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    symbol: {
        type: Sequelize.STRING,
        allowNull: false
    },
    shares: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    price: {
        type: Sequelize.DECIMAL,
        allowNull: false
    }
}, {
    indexes: [
        {
            unique: false,
            fields: ['user_id', 'type']
        }
    ],
    timestamps: false
}
);

module.exports = Trade;