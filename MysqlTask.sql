Run query steps wise before that you need to create database 

CREATE DATABASE `code_ngine_task`;

-- create table 
step1 -: players table
    
    CREATE TABLE`code_ngine_task`.`players`(`id` INT NOT NULL AUTO_INCREMENT, `name` TEXT NOT NULL, PRIMARY KEY(`id`)) ENGINE = InnoDB;

step2 -: countries table
    
    CREATE TABLE`code_ngine_task`.`countries`(`id` INT NOT NULL AUTO_INCREMENT, `name` VARCHAR(50) NOT NULL, PRIMARY KEY(`id`), UNIQUE(`name`)) ENGINE = InnoDB;

Step3 -: goals table

CREATE TABLE`code_ngine_task`.`goals`
    (
        `player_id` INT NOT NULL, PRIMARY KEY(`player_id`),
        `country_id` INT NOT NULL,
        `goals` INT NOT NULL,
        FOREIGN KEY(player_id) REFERENCES players(id),
        FOREIGN KEY(country_id) REFERENCES countries(id)

    ) ENGINE = InnoDB;

-- insert data

step4 -:

insert data of PLAYERS 


INSERT INTO players(name) VALUES('cristiano'), ('messi'), ('toni'), ('silva'), ('Mbappe'), ('marco'), ('greizmann'), ('dybala'), ('werner')

insert data of countries 

INSERT INTO countries(name) VALUES('Germany'), ('portugal'), ('argentina'), ('france')

insert data of goals 

INSERT INTO goals(player_id, country_id, goals) VALUES(1,2,6), (2,3,6), (3,1,5), (4,2,3), (5,4,7), (6,1,3), (7,4,8), (8,3,4), (9,1,3)



step5 -:
To get expected output

SELECT c.name as countryName, sum(g.goals) as goalsScored
FROM countries as c, goals as g
Where c.id = g.country_id
GROUP BY c.id
ORDER BY goalsScored DESC

Above the query Getting 0.0019seconds time 

SELECT c.name as countryName, sum(g.goals) as goalsScored
FROM  goals as g
JOIN countries as c ON c.id = g.country_id
GROUP BY c.id
ORDER BY goalsScored DESC

above the query Getting 0.0018seconds time

not much difference between two queries because low data