1.API URL
localhost:5000/trades
body
    {
        
        "Type": "BUY",
        "User_id": 1,
         "symbol" : "ABX",
        "Shares": 30,
        "Price": 52
    }

2.GET ALL TRADES API 
localhost:5000/trades?type=buy&user_id=1 

3.GET TRADE BY ID 
localhost:5000/trades/5

3.DELETE TRADE BY ID 
localhost:5000/trades/5

4.UPDATE REQUEST BY ID 
{
        
    "Type": "SELL",
    "User_id": 1,
        "symbol" : "ABX",
    "Shares": 80,
    "Price": 52
}