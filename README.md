# TradesAPI



## Getting started
To run the App need to set .env file in project root level with database credentials
like this 

```
    HOST="sql11.freemysqlhosting.net"
    DATABASE="sql11452210"
    USERNAME="sql11452210"
    PASSWORD="gJHW7FKcxZ"
    DATABASE_PORT=3306
```

**_Note:-Table will be automatically created after running the App._**

To run the app get clone of the project
```
    set .env file 
    npm install 
    npm run dev for Development
    npm start for Productions
```
URL 
1.API URL
localhost:5000/trades
body
```
    {
        
        "type": "BUY",
        "user_id": 1,
        "symbol" : "ABX",
        "shares": 30,
        "price": 52
    }
```
2.GET ALL TRADES API 
localhost:5000/trades?type=buy&user_id=1 

3.GET TRADE BY ID 
localhost:5000/trades/5

3.DELETE TRADE BY ID 
localhost:5000/trades/5

4.UPDATE REQUEST BY ID 
```
{
        
    "type": "SELL",
    "user_id": 1,
    "symbol" : "ABX",
    "shares": 80,
    "price": 52
}
```

PRODUCTION URL :https://trades-api-v1.herokuapp.com/trades/


**MYSQL task on the MysqlTask.sql file follow the setps.** 

