const tradeDao = require('../dao/trade.dao');
const Op = require('sequelize').Op

var tradeController = {
    addTrade: addTrade,
    findTrades: findTrades,
    findTradeById: findTradeById,
    updateTrade: updateTrade,
    deleteById: deleteById
}

function addTrade(req, res) {
    let trade = req.body;
    // validations 
    let shares = trade.shares && typeof trade.shares === "number" ? parseInt(trade.shares) : null; 
    if (!shares) {
        return res.status(400).send("Shares value will be number")
    } else if(shares && !(0 < shares && shares < 101)){
        return res.status(400).send("Shares value will be number range in 1-100")
    } 
    let type = trade.type && typeof trade.type === "string" && ["SELL", "BUY"].includes((trade.type).toUpperCase()) ? (trade.type) : null;
    if (!type) {
        return res.status(400).send("Type value will be sell or buy");
    }
    tradeDao.create(trade).
        then((data) => {
            return res.status(201).send(data);
        })
        .catch((error) => {
            return res.status(400).send(error.message)
        });  
}

function findTradeById(req, res) {
    tradeDao.findById(req.params.id).
        then((data) => {
            res.status(200).send(data);
        })
        .catch((error) => {
            res.status(404).send(error.message)
        });
}

function deleteById(req, res) {
    tradeDao.deleteById(req.params.id).
        then((data) => {
            res.status(200).json({
                message: "User deleted successfully",
                trade: data
            })
        })
        .catch((error) => {
            res.status(405).send(error.message)
        });
}

function updateTrade(req, res) {
    let trade = req.body;
    // validations 
    let shares = trade.shares && typeof trade.shares === "number" ? parseInt(trade.shares) : null;
   if (shares && !(0 < shares && shares < 101)) {
        return res.status(405).send("Shares value will be number range in 1-100")
    }
    let type = trade.type && typeof trade.type === "string" ? trade.type : null;
    if (type && !["SELL", "BUY"].includes((type).toUpperCase())) {
        return res.status(405).send("Type value will be sell or buy");
    }
    tradeDao.updateTrade(req.body, req.params.id).
        then((data) => {
            res.status(200).json({
                message: "User updated successfully",
                trade: data
            })
        })
        .catch((error) => {
            res.status(405).send(error.message)
        });
}

function findTrades(req, res) {
    const type = (req.query.type && typeof req.query.type === "string") ? req.query.type.toUpperCase() : "";
    const user_id = (req.query.user_id) ? Number(req.query.user_id) : null;
    let filters = { where: {} };
    if (type) {
        filters.where.type = { ...{[Op.eq]: type} }
    }
    if (user_id) {
        filters.where.user_id = { ...{[Op.eq]: user_id} }
    }
    tradeDao.findAll(filters).
        then((data) => {
            res.status(200).send(data);
        })
        .catch((error) => {
            res.status(400).send(error.message)
        });
}

module.exports = tradeController;