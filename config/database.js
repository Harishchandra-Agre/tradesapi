require("dotenv").config()
const { Sequelize } = require('sequelize');
const HOST = process.env.HOST;
const DATABASE = process.env.DATABASE;
const USERNAME = process.env.USERNAME;
const PASSWORD = process.env.PASSWORD;
const DATABASE_PORT = process.env.DATABASE_PORT;
const db = new Sequelize(DATABASE, USERNAME, PASSWORD, {
    host: HOST,
    dialect: 'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    port: DATABASE_PORT
});
module.exports = db;
