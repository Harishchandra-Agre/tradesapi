const Trade = require('../models/Trade');
let tradeDao = {
    findAll: findAll,
    create: create,
    findById: findById,
    deleteById: deleteById,
    updateTrade: updateTrade
}

function findAll(filters) {
    return Trade.findAll(filters);
}

function findById(id) {
    return Trade.findByPk(id);
}

function deleteById(id) {
    return Trade.destroy({ where: { id: id } });
}

function create(trade) {
    let newTrade = new Trade({
        type: trade.type,
        user_id: trade.user_id,
        symbol: trade.symbol,
        shares: trade.shares,
        price: trade.price,
    });
    return newTrade.save();
}

function updateTrade(trade, id) {
    let updateTrade = {
        type: trade.type,
        user_id: trade.user_id,
        symbol: trade.symbol,
        shares: trade.shares,
        price: trade.price,
    };
    return Trade.update(updateTrade, { where: { id: id } });
}
module.exports = tradeDao;
